# DynamicMOTD

# Setup
```
sudo apt-get install net-tools -y

if [ ! -f /opt/UnixIdentification.sh ]; then
  mkdir -p /opt
  curl -fsSL -o /opt/UnixIdentification.sh https://gitlab.com/Kittell-Projects/Unix/unixidentification/-/raw/master/UnixIdentification.sh
fi
sudo mkdir -p /etc/update-motd.d
sudo curl -fsSL -o /etc/update-motd.d/11-server-info https://gitlab.com/Kittell-Projects/Unix/dynamicmotd/-/raw/master/11-server-info
sudo chmod -x /etc/update-motd.d/*
sudo chmod +x /etc/update-motd.d/11-server-info
sudo rm /etc/motd
sudo ln -s /var/run/motd /etc/motd
```

